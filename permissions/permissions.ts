export const MEDICALPRACTICE_PERMISSIONS = {
    PATIENT: {
        //CRUD PERMISSIONS
        READ: 'medicalpractice@Patient/read',
        CREATE: 'medicalpractice@Patient/create',
        UPDATE: 'medicalpractice@Patient/update',
        DELETE: 'medicalpractice@Patient/delete',
        //CUSTOM PERMISSIONS
        CUSTOM: {
            GETSOMEDATA: 'medicalpractice@Patient/getSomeData',
        },
    },
    APPOINTMENT: {
        //CRUD PERMISSIONS
        READ: 'medicalpractice@Appointment/read',
        CREATE: 'medicalpractice@Appointment/create',
        UPDATE: 'medicalpractice@Appointment/update',
        DELETE: 'medicalpractice@Appointment/delete',
        //CUSTOM PERMISSIONS
        CUSTOM: {
            DOWHATEVER: 'medicalpractice@Appointment/doWhatever',
            DOTHIS: 'medicalpractice@Appointment/doThis',
            DOTHAT: 'medicalpractice@Appointment/doThat',
        },
    },
    REPORT: {
        //CRUD PERMISSIONS
        READ: 'medicalpractice@Report/read',
        CREATE: 'medicalpractice@Report/create',
        UPDATE: 'medicalpractice@Report/update',
        DELETE: 'medicalpractice@Report/delete',
        //CUSTOM PERMISSIONS
        CUSTOM: {},
    },
    TASK: {
        //CRUD PERMISSIONS
        READ: 'medicalpractice@Task/read',
        CREATE: 'medicalpractice@Task/create',
        UPDATE: 'medicalpractice@Task/update',
        DELETE: 'medicalpractice@Task/delete',
        //CUSTOM PERMISSIONS
        CUSTOM: {},
    },
    ROOM: {
        //CRUD PERMISSIONS
        READ: 'medicalpractice@Room/read',
        CREATE: 'medicalpractice@Room/create',
        UPDATE: 'medicalpractice@Room/update',
        DELETE: 'medicalpractice@Room/delete',
        //CUSTOM PERMISSIONS
        CUSTOM: {},
    },
    DOCTOR: {
        //CRUD PERMISSIONS
        READ: 'medicalpractice@Doctor/read',
        CREATE: 'medicalpractice@Doctor/create',
        UPDATE: 'medicalpractice@Doctor/update',
        DELETE: 'medicalpractice@Doctor/delete',
        //CUSTOM PERMISSIONS
        CUSTOM: {},
    },
    NURSE: {
        //CRUD PERMISSIONS
        READ: 'medicalpractice@Nurse/read',
        CREATE: 'medicalpractice@Nurse/create',
        UPDATE: 'medicalpractice@Nurse/update',
        DELETE: 'medicalpractice@Nurse/delete',
        //CUSTOM PERMISSIONS
        CUSTOM: {},
    },
    RECEPTIONIST: {
        //CRUD PERMISSIONS
        READ: 'medicalpractice@Receptionist/read',
        CREATE: 'medicalpractice@Receptionist/create',
        UPDATE: 'medicalpractice@Receptionist/update',
        DELETE: 'medicalpractice@Receptionist/delete',
        //CUSTOM PERMISSIONS
        CUSTOM: {},
    },
};
