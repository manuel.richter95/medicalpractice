import { Doctor } from './Doctor';
import { DoctorBaseController } from './base/DoctorBaseController';
import { DoctorService } from './DoctorService';
import { ModelController } from '@core/api/decorators';

@ModelController('Doctor')
export class DoctorController extends DoctorBaseController {
    constructor(public doctorService: DoctorService) {
        super(doctorService);
    }
}
