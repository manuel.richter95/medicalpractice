import { model } from 'mongoose';
import { DoctorBaseDocument, DoctorBaseSchema } from './base/DoctorBase';

export interface DoctorDocument extends DoctorBaseDocument {}

export var DoctorSchema = DoctorBaseSchema;
export const Doctor = model<DoctorDocument>('Doctor', DoctorSchema);
