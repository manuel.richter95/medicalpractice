import { Injectable } from '@decorators/di';
import { BaseService } from '@core/api/BaseService';
import { Doctor } from './Doctor';

@Injectable()
export class DoctorService extends BaseService {
    constructor() {
        super(Doctor);
    }
}
