import { Injectable } from '@decorators/di';
import { BaseService } from '@core/api/BaseService';
import { Room } from './Room';

@Injectable()
export class RoomService extends BaseService {
    constructor() {
        super(Room);
    }
}
