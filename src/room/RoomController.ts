import { Room } from './Room';
import { RoomBaseController } from './base/RoomBaseController';
import { RoomService } from './RoomService';
import { ModelController } from '@core/api/decorators';

@ModelController('Room')
export class RoomController extends RoomBaseController {
    constructor(public roomService: RoomService) {
        super(roomService);
    }
}
