import { model } from 'mongoose';
import { RoomBaseDocument, RoomBaseSchema } from './base/RoomBase';

export interface RoomDocument extends RoomBaseDocument {}

export var RoomSchema = RoomBaseSchema;
export const Room = model<RoomDocument>('Room', RoomSchema);
