import { Injectable } from '@decorators/di';
import { BaseService } from '@core/api/BaseService';
import { Task } from './Task';

@Injectable()
export class TaskService extends BaseService {
    constructor() {
        super(Task);
    }
}
