import { model } from 'mongoose';
import { TaskBaseDocument, TaskBaseSchema } from './base/TaskBase';

export interface TaskDocument extends TaskBaseDocument {}

export var TaskSchema = TaskBaseSchema;
export const Task = model<TaskDocument>('Task', TaskSchema);
