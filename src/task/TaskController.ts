import { Task } from './Task';
import { TaskBaseController } from './base/TaskBaseController';
import { TaskService } from './TaskService';
import { ModelController } from '@core/api/decorators';

@ModelController('Task')
export class TaskController extends TaskBaseController {
    constructor(public taskService: TaskService) {
        super(taskService);
    }
}
