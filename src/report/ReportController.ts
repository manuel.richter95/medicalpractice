import { Report } from './Report';
import { ReportBaseController } from './base/ReportBaseController';
import { ReportService } from './ReportService';
import { ModelController } from '@core/api/decorators';

@ModelController('Report')
export class ReportController extends ReportBaseController {
    constructor(public reportService: ReportService) {
        super(reportService);
    }
}
