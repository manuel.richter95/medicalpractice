import { Injectable } from '@decorators/di';
import { BaseService } from '@core/api/BaseService';
import { Report } from './Report';

@Injectable()
export class ReportService extends BaseService {
    constructor() {
        super(Report);
    }
}
