import { model } from 'mongoose';
import { ReportBaseDocument, ReportBaseSchema } from './base/ReportBase';

export interface ReportDocument extends ReportBaseDocument {}

export var ReportSchema = ReportBaseSchema;
export const Report = model<ReportDocument>('Report', ReportSchema);
