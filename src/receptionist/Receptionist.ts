import { model } from 'mongoose';
import { ReceptionistBaseDocument, ReceptionistBaseSchema } from './base/ReceptionistBase';

export interface ReceptionistDocument extends ReceptionistBaseDocument {}

export var ReceptionistSchema = ReceptionistBaseSchema;
export const Receptionist = model<ReceptionistDocument>('Receptionist', ReceptionistSchema);
