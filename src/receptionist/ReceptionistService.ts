import { Injectable } from '@decorators/di';
import { BaseService } from '@core/api/BaseService';
import { Receptionist } from './Receptionist';

@Injectable()
export class ReceptionistService extends BaseService {
    constructor() {
        super(Receptionist);
    }
}
