import { Receptionist } from './Receptionist';
import { ReceptionistBaseController } from './base/ReceptionistBaseController';
import { ReceptionistService } from './ReceptionistService';
import { ModelController } from '@core/api/decorators';

@ModelController('Receptionist')
export class ReceptionistController extends ReceptionistBaseController {
    constructor(public receptionistService: ReceptionistService) {
        super(receptionistService);
    }
}
