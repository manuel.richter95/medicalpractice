import { Patient } from './Patient';
import { PatientBaseController } from './base/PatientBaseController';
import { PatientService } from './PatientService';
import { ModelController } from '@core/api/decorators';

@ModelController('Patient')
export class PatientController extends PatientBaseController {
    constructor(public patientService: PatientService) {
        super(patientService);
    }
}
