import { Injectable } from '@decorators/di';
import { BaseService } from '@core/api/BaseService';
import { Patient } from './Patient';

@Injectable()
export class PatientService extends BaseService {
    constructor() {
        super(Patient);
    }
}
