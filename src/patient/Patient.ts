import { model } from 'mongoose';
import { PatientBaseDocument, PatientBaseSchema } from './base/PatientBase';

export interface PatientDocument extends PatientBaseDocument {}

export var PatientSchema = PatientBaseSchema;
export const Patient = model<PatientDocument>('Patient', PatientSchema);
