import { Nurse } from './Nurse';
import { NurseBaseController } from './base/NurseBaseController';
import { NurseService } from './NurseService';
import { ModelController } from '@core/api/decorators';

@ModelController('Nurse')
export class NurseController extends NurseBaseController {
    constructor(public nurseService: NurseService) {
        super(nurseService);
    }
}
