import { Injectable } from '@decorators/di';
import { BaseService } from '@core/api/BaseService';
import { Nurse } from './Nurse';

@Injectable()
export class NurseService extends BaseService {
    constructor() {
        super(Nurse);
    }
}
