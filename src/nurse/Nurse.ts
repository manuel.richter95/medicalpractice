import { model } from 'mongoose';
import { NurseBaseDocument, NurseBaseSchema } from './base/NurseBase';

export interface NurseDocument extends NurseBaseDocument {}

export var NurseSchema = NurseBaseSchema;
export const Nurse = model<NurseDocument>('Nurse', NurseSchema);
