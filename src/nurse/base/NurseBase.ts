//THIS FILE IS AUTOGENERATED
//DO NOT MODIFY
import { Schema } from 'mongoose';
import { BaseDocument } from '@core/api/BaseDocument';
import { ModelSchema } from '@core/orm/schema/ModelSchema';

export interface NurseBaseDocument extends BaseDocument {
    name?: string[];
    address?: [{ street: string; houseNumber?: number[] }];
    sub?: { foo?: string[]; bar?: number; subsub?: [{ subsubfoo?: string; subsubbar?: number[] }] };
}

export const NurseBaseSchema: ModelSchema = new ModelSchema({
    name: {
        type: [String],
        required: false,
        immutable: false,
        unique: false,
        select: true,
        array: true,
        writable: true,
        readable: true,
        defaultColumn: false,
        listed: true,
    },
    address: {
        type: [
            new Schema(
                {
                    street: {
                        type: String,
                        required: true,
                        immutable: false,
                        unique: false,
                        select: true,
                        array: false,
                        writable: true,
                        readable: true,
                        defaultColumn: false,
                        listed: true,
                        maxLength: 255,
                    },
                    houseNumber: {
                        type: [Number],
                        required: false,
                        immutable: false,
                        unique: false,
                        select: true,
                        array: true,
                        writable: true,
                        readable: true,
                        defaultColumn: false,
                        listed: true,
                        maxLength: 10,
                    },
                },
                { _id: false }
            ),
        ],
        required: false,
        immutable: false,
        unique: false,
        select: true,
        array: true,
        writable: true,
        readable: true,
        defaultColumn: false,
        listed: true,
        minItems: 3,
    },
    sub: {
        foo: {
            type: [String],
            required: false,
            immutable: false,
            unique: false,
            select: true,
            array: true,
            writable: true,
            readable: true,
            defaultColumn: false,
            listed: true,
            maxLength: 255,
        },
        bar: {
            type: Number,
            required: false,
            immutable: false,
            unique: false,
            select: true,
            array: false,
            writable: true,
            readable: true,
            defaultColumn: false,
            listed: true,
            min: 5,
        },
        subsub: {
            type: [
                new Schema(
                    {
                        subsubfoo: {
                            type: String,
                            required: false,
                            immutable: false,
                            unique: false,
                            select: true,
                            array: false,
                            writable: true,
                            readable: true,
                            defaultColumn: false,
                            listed: true,
                            maxLength: 255,
                        },
                        subsubbar: {
                            type: [Number],
                            required: false,
                            immutable: false,
                            unique: false,
                            select: true,
                            array: true,
                            writable: true,
                            readable: true,
                            defaultColumn: false,
                            listed: true,
                        },
                    },
                    { _id: false }
                ),
            ],
            required: false,
            immutable: false,
            unique: false,
            select: true,
            array: true,
            writable: true,
            readable: true,
            defaultColumn: false,
            listed: true,
        },
    },
});

NurseBaseSchema.methods.getDisplayName = function () {
    return this._id;
};

NurseBaseSchema.statics.conditions = {
    required: {},
    disabled: {},
    validate: {},
};
