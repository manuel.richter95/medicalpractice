//THIS FILE IS AUTOGENERATED
//DO NOT MODIFY
import { Schema } from 'mongoose';
import { BaseDocument } from '@core/api/BaseDocument';
import { ModelSchema } from '@core/orm/schema/ModelSchema';
import { PatientDocument } from '@modules/medicalpractice';
import { UserDocument } from '@models/user';

export interface AppointmentBaseDocument extends BaseDocument {
    title: string;
    description?: string;
    startTime: Date;
    endTime?: Date;
    reasons?: string[];
    note?: string;
    someNumber?: number;
    hasArrived?: boolean;
    patient?: PatientDocument;
    doctors?: UserDocument[];
}

export const AppointmentBaseSchema: ModelSchema = new ModelSchema({
    title: {
        type: String,
        required: true,
        immutable: false,
        unique: false,
        select: true,
        array: false,
        writable: true,
        readable: true,
        defaultColumn: false,
        listed: true,
        maxLength: 255,
    },
    description: {
        type: String,
        required: false,
        immutable: false,
        unique: false,
        select: true,
        array: false,
        writable: true,
        readable: true,
        defaultColumn: false,
        listed: true,
        maxLength: 10000,
    },
    startTime: {
        type: Date,
        required: true,
        immutable: false,
        unique: false,
        select: true,
        array: false,
        writable: true,
        readable: true,
        defaultColumn: false,
        listed: true,
    },
    endTime: {
        type: Date,
        required: false,
        immutable: false,
        unique: false,
        select: true,
        array: false,
        writable: true,
        readable: true,
        defaultColumn: false,
        listed: true,
    },
    reasons: {
        type: [String],
        required: false,
        immutable: false,
        unique: false,
        select: true,
        array: true,
        writable: true,
        readable: true,
        defaultColumn: false,
        listed: true,
        maxLength: 255,
    },
    note: {
        type: String,
        required: false,
        immutable: false,
        unique: false,
        select: true,
        array: false,
        writable: true,
        readable: true,
        defaultColumn: false,
        listed: true,
        maxLength: 10000,
    },
    someNumber: {
        type: Number,
        required: false,
        immutable: false,
        unique: false,
        select: true,
        array: false,
        writable: true,
        readable: true,
        defaultColumn: false,
        listed: true,
    },
    hasArrived: {
        type: Boolean,
        required: false,
        immutable: false,
        unique: false,
        select: true,
        array: false,
        writable: true,
        readable: true,
        defaultColumn: false,
        listed: true,
    },
    patient: {
        type: Schema.Types.ObjectId,
        required: false,
        immutable: false,
        unique: false,
        select: true,
        array: false,
        ref: 'Patient',
        writable: true,
        readable: true,
        defaultColumn: false,
        listed: true,
    },
    doctors: {
        type: [{ type: Schema.Types.ObjectId, ref: 'User' }],
        required: false,
        immutable: false,
        unique: false,
        select: true,
        array: true,
        writable: true,
        readable: true,
        defaultColumn: false,
        listed: true,
    },
});

AppointmentBaseSchema.methods.getDisplayName = function () {
    return this.title;
};

AppointmentBaseSchema.statics.conditions = {
    required: {},
    disabled: {},
    validate: {},
};
