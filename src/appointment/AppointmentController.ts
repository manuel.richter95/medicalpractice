import { Appointment } from './Appointment';
import { AppointmentBaseController } from './base/AppointmentBaseController';
import { AppointmentService } from './AppointmentService';
import { ModelController } from '@core/api/decorators';

@ModelController('Appointment')
export class AppointmentController extends AppointmentBaseController {
    constructor(public appointmentService: AppointmentService) {
        super(appointmentService);
    }
}
