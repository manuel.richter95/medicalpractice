import { model } from 'mongoose';
import { AppointmentBaseDocument, AppointmentBaseSchema } from './base/AppointmentBase';

export interface AppointmentDocument extends AppointmentBaseDocument {}

export var AppointmentSchema = AppointmentBaseSchema;
export const Appointment = model<AppointmentDocument>('Appointment', AppointmentSchema);
