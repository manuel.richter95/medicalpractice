import { Injectable } from '@decorators/di';
import { BaseService } from '@core/api/BaseService';
import { Appointment } from './Appointment';

@Injectable()
export class AppointmentService extends BaseService {
    constructor() {
        super(Appointment);
    }
}
